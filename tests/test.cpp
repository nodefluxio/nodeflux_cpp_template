#define CATCH_CONFIG_MAIN
#include "catch2/catch.hpp"
#include "nodeflux_cpp_template/common.hpp"


// quick hack because somehow Catch2 will miss libasyik's test cases if
// we dont do this:
TEST_CASE("Invoke all test units", "[test]")
{
  REQUIRE(test()==true);
}
